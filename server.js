const express = require("express");
const { getDb, connectToDb } = require("./db");
const { ObjectId } = require("mongodb")


const PORT = 3000;
const app =  express();
app.use(express.json());

let db;

connectToDb((err) => {
    if (!err) {
      app.listen(PORT, (err) => {
        err ? console.log(err) : console.log(`listening port ${PORT}`);
      });
      db = getDb();
    } else {
      console.log(`DB connection error: ${err}`);
    }
  });
  
  const handleError = (res, error) => {
    res.status(500).json({ error });
  }

  app.get('/data', (req, res) => {
    const data = [];
    db
      .collection('data')
      .find()
      .sort({ title: 1 })
      .forEach((movie) => data.push(movie))
      .then(() => {
        res
          .status(200)
          .json(data);
      })
      .catch(() => handleError(res,"Something goes wrong..." ))
  });


  app.get('/data/:id', (req, res) => {
    if (ObjectId.isValid(req.params.id)) {
      
      db
      .collection('data')
      .findOne({ _id: new ObjectId(req.params.id) })
      .then((doc) => {
        console.log(doc);
        res
          .status(200)
          .json(doc);
      })
      .catch(() => handleError(res,"Something goes wrong..." ))
    } else {
      handleError(res,"Wrong id")
    }
  });

  app.delete('/data/:id', (req, res) => {
    if (ObjectId.isValid(req.params.id)) {
      
      db
      .collection('data')
      .deleteOne({ _id: new ObjectId(req.params.id) })
      .then((result) => {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(() => handleError(res,"Something goes wrong..." ))
    } else {
      handleError(res,"Wrong id")
    }
  });

  app.post("/data",(req,res)=>{
    db
    .collection('data')
    .insertOne(req.body)
    .then((result) => {
      console.log(result);
      res
        .status(201)
        .json(result);
    })
    .catch(() => handleError(res,"Something goes wrong..." ))
  })

  app.patch("/data/:id",(req,res)=>{
    if (ObjectId.isValid(req.params.id)) {
      
      db
      .collection('data')
      .updateOne({ _id: new ObjectId(req.params.id) },{$set:req.body})
      .then((result) => {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(() => handleError(res,"Something goes wrong..." ))
    } else {
      handleError(res,"Wrong id")
    }
  })